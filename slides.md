% GitLab CI
% Winnie Hellmann
% 2019-04-14

## Was ist CI?

- ein InterCity rückwärts
- manchmal auch Corporate Identity
- hier aber: **Continuous Integration**

---

## Wie sieht CI aus?


![many small characters running around busy with different tasks](infinite_battle.gif)

---

## Was macht CI?

- automatisierte Tests
- statische Code-Analyse
  - potentielle Bugs
  - Sicherheitslücken
  - Lizenzprobleme
- Code-Style prüfen

---

## Wie geht CI?

- in GitLab per Konfiguration in `.gitlab-ci.yml`
- üblicherweise eine pro Projekt
- prinzipiell eine pro Branch bzw. Commit möglich

---

## Wie geht CI?

- im einfachsten Fall:

```yaml
mein job:
  script:
    - echo "Nichts tun."
```

![screenshot of pipeline with one job](small-pipeline.png)

---

## Wie geht CI?

![screenshot of job log](small-pipeline-log.png)

---

## Wie geht Parallelität?

```yaml
mein job:
  script:
    - echo "Nichts tun."

dein job:
  script:
    - echo "Viel tun."
```

![screenshot of parallel pipeline jobs](parallel-jobs.png)

---

## Wie geht Parallelität?

```yaml
dein job:
  parallel: 5
  script:
    - echo "Aufgabe ${CI_NODE_INDEX} von ${CI_NODE_TOTAL}"
```

![screenshot of parallel pipeline jobs](many-parallel-jobs.png)

---

## Wie geht Unparallelität?

```yaml
mein job:
  stage: test
  script:
    - echo "Nichts tun."

dein job:
  stage: deploy
  script:
    - echo "Viel tun."
```

![screenshot of pipeline with two stages](two-stages.png)

---

## Wie geht Unparallelität?

```yaml
stages:
  - zuerst
  - zuzweit

mein job:
  stage: zuerst
  script:
    - echo "Nichts tun."

dein job:
  stage: zuzweit
  script:
    - echo "Viel tun."
```

---

## Wie entstehen neue Pipelines?

- der Storch bringt sie nicht
- jeder `git push` erzeugt eine neue Pipeline
- Pipelines können [manuell erzeugt werden](https://docs.gitlab.com/ee/ci/pipelines.html#manually-executing-pipelines)
- oder [per API](https://docs.gitlab.com/ee/api/pipelines.html#create-a-new-pipeline)
- cron-artige [Pipeline schedules](https://docs.gitlab.com/ee/user/project/pipelines/schedules.html)

---

## Trödeln

```yaml
moment:
  when: delayed
  start_in: 30 minutes
  script:
    - echo "Wer hat an der Uhr gedreht?"
```

[https://docs.gitlab.com/ee/ci/yaml/#whendelayed](https://docs.gitlab.com/ee/ci/yaml/#whendelayed)

---

## Handarbeit

```yaml
galvanize:
  when: manual
  script:
    - echo "Push the button!"
```

![screenshot of manual pipeline job](manual-job.png)

[https://docs.gitlab.com/ee/ci/yaml/#whenmanual](https://docs.gitlab.com/ee/ci/yaml/#whenmanual)

---

## Inklusion und Exklusion

```yaml
harte Arbeit:
  script:
    - echo "Schaffe, schaffe, Häusle baue"
  except:
    - master
  only:
    - apprentice
```

[https://docs.gitlab.com/ee/ci/yaml/#onlyexcept-basic](https://docs.gitlab.com/ee/ci/yaml/#onlyexcept-basic)

---

## Advanced Awesomeness

- [Variablen](https://docs.gitlab.com/ee/ci/yaml/#variables): Verhalten und Geheimnisse
- [Artefakte](https://docs.gitlab.com/ee/ci/yaml/#artifacts): Dateien ausspucken
- [Caching](https://docs.gitlab.com/ee/ci/yaml/#cache): Dateien zwischen Pipelines austauschen 
- [Extend](https://docs.gitlab.com/ee/ci/yaml/#extends): Komplexe Jobs bauen
- [Include](https://docs.gitlab.com/ee/ci/yaml/#include): Komplexe Konfigurationen bauen


---

## CI mit CI vom <del>CI</del> IC

![pipeline with white and red stripe](ci.png)
